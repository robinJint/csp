import Test.Tasty
import Test.Tasty.HUnit

import Data.Either (isRight, isLeft)
import Csp (runFile)

main = defaultMain tests

assertParseFile :: HasCallStack => String -> Assertion
assertParseFile file = assertBool file . isRight =<< runFile ("examples/" ++ file ++ ".csp")

assertParseFileFail :: HasCallStack => String -> Assertion
assertParseFileFail file = assertBool file . isLeft =<< runFile ("examples/" ++ file ++ ".csp")

tests :: TestTree
tests = testGroup "Integration tests" [examples, ai]

examples = testGroup "files in the examples directory" $
    (map (\x -> testCase x $ assertParseFile x)
        [ "simple"
        ]) ++
    (map (\x -> testCase x $ assertParseFileFail x)
        [ "double"
        , "empty"
        , "intconstr"
        ])

ai = testGroup "files from the ai course (slightly modified)" $
    map (\x -> testCase x $ assertParseFile x)
    [ "AI/chain"
    , "AI/chainA"
    , "AI/chainZ"
    , "AI/cryptarithmeticeighty"
    , "AI/cryptarithmetichurts"
    , "AI/cryptarithmeticmoney"
    , "AI/cryptarithmeticonze"
    , "AI/equations"
    , "AI/market"
    , "AI/primes"
    , "AI/problem"
    , "AI/queens10"
    , "AI/queens5"
    , "AI/queens6"
    , "AI/queens7"
    , "AI/queens8"
    , "AI/queens9"
    , "AI/sudoku"
    , "AI/sudoku_2"
    , "AI/sudoku_petra"
    , "AI/sudoku_translated"
    , "AI/sudokuhard"
    , "AI/sudokusmiley"
    , "AI/translation"

    ]
