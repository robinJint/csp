{
{-# LANGUAGE OverloadedStrings #-}

module Csp.Parser where

import Data.Char

import Control.Monad.Except
import qualified Data.ByteString.Lazy as BL
import qualified Data.ByteString.Lazy.Char8 as BL8
import Data.Char (ord)
import Csp.Lexer
import Csp.Ast
import Csp.Base
import Csp.Syntax
import Control.Monad.Except

}

%name parseCSP
%tokentype { Token }

%monad { Parser } { thenP } { returnP }
%lexer { lexer } { Token _ TokenEOF }
%errorhandlertype explist
%error { parseError }

%token
        let             { Token _ TokenLet      }
        csp             { Token _ TokenCsp      }
        begin             { Token _ TokenBegin      }
        end             { Token _ TokenEnd      }
        in              { Token _ TokenIn       }
        input              { Token _ TokenInput       }
        output              { Token _ TokenOutput       }
        variables              { Token _ TokenVariables       }
        domains              { Token _ TokenDomains       }
        constaints              { Token _ TokenConstraints       }
        solutions              { Token _ TokenSolutions       }
        solve              { Token _ TokenSolve       }
        int             { Token _ (TokenInt $$) }
        var             { Token _ (TokenVar $$) }
        true             { Token _ TokenTrue }
        false             { Token _ TokenFalse }
        all              { Token _ TokenAll       }
        forall              { Token _ TokenForall       }
        div              { Token _ TokenDiv       }
        mod              { Token _ TokenMod       }
        '+'             { Token _ TokenPlus     }
        '-'             { Token _ TokenMinus    }
        '*'             { Token _ TokenTimes    }
        '/'             { Token _ TokenDiv      }
        '='             { Token _ TokenEQ}
        '!='             { Token _ TokenNEQ       }
        '!'             { Token _ TokenNOT       }
        '>'             { Token _ TokenGT       }
        '>='             { Token _ TokenGE       }
        '<'             { Token _ TokenLT       }
        '<='             { Token _ TokenLE       }
        '('             { Token _ TokenOB       }
        ')'             { Token _ TokenCB       }
        '['             { Token _ TokenOSB       }
        ']'             { Token _ TokenCSB       }
        ':'             { Token _ TokenColon    }
        ';'             { Token _ TokenSemiColon    }
        '..'             { Token _ TokenDoubleDot    }
        '<-'             { Token _ TokenArrowLeft    }
        ','             { Token _ TokenComma    }

%%

Csps :: {[Toplevel]}
    : {[]}
    | Csps Csp { $2:$1 }

Csp :: {Toplevel}
    : csp Name '(' Input Output ')' begin Variables Domains Constraints Solutions end lineno {
            Problem {
                name=$2,
                input=$4,
                output=$5,
                variables=$8,
                domains=$9,
                constraints=$10,
                solutions=$11
            }
        }

Input :: {[([(String, AlexPosn)], Type)]}
      : { [] }
      | input ':' Decls {$3}

Output :: {[([(String, AlexPosn)], Type)]}
      : { [] }
      | output ':' Decls {$3}

Variables :: {[([(String, AlexPosn)], Type)]}
      : { [] }
      | variables ':' Decls {$3}

Decls :: {[([(String, AlexPosn)],Type)]}
      : {- empty -} { [] }
      | Decls Decl { $2:$1 }

Decl :: {([(String, AlexPosn)], Type)}
    : Vars ':' Type ';' {($1, $3)}

Vars :: {[(String, AlexPosn)]}
    : Name {[$1]}
    | Vars ',' Name {$3:$1}

Domains :: {[SDomain]}
    : { [] }
    | domains ':' DomainDecls {$3}

DomainDecls :: {[SDomain]}
    : {- empty -} { [] }
    | DomainDecls DomainDecl {$2:$1}

DomainDecl :: {SDomain}
    : DomainSpecifiers '<-' '[' ExprList ']' ';' { Domain $1 $4 }
    | forall '(' Name in '[' ExprRangeList ']' ')' DomainDecls end { ForallDomain $3 $6 $9}

DomainSpecifiers :: {[SDomainSpecifier]}
    : DomainSpecifier {[$1]}
    | DomainSpecifiers ',' DomainSpecifier {$3:$1}

DomainSpecifier :: {SDomainSpecifier}
    : lineno var {DomainVar ($2, $1)}
    | DomainSpecifier '[' Expr ']' {DomainIndex $1 $3}

Constraints :: {[Expr]}
    : {- empty -} { [] }
    | constaints ':' ExprBlock { $3 }

ExprBlock :: {[Expr]}
    : {- empty -} { [] }
    | ExprBlock Expr ';' { $2:$1 }
    | ExprBlock solve lineno var '(' ExprList ')' ';' { Expr1 $3 (SSolve $4 $6):$1 }
    | ExprBlock forall '(' Name in '[' ExprList ']' ')' ExprBlock end 
        {% alexGetPosition >>= (\x -> pure $ Expr1 x (SForall $4 $7 $10):$1)}

Type :: {Type}
    : var {Type $1}
    | '[' Type ';' int ']' {TypeArray $2 $4 }

ExprList :: {[Expr]}
    : {[]}
    | Expr {[$1]}
    | ExprList ',' Expr {$3:$1}

Expr :: {Expr}
    : lineno Expr1 {Expr1 $1 $2}
    | lineno Expr1 lineno BinOp Expr { BinOp $3 $4 $1 $2 $5}

ExprRangeList :: {Expr}
    : lineno Expr1 {Expr1 $1 $2}
    | lineno Expr1 lineno BinOpRangeConcat ExprRangeList { BinOp $3 $4 $1 $2 $5}

Expr1 :: {Expr1}
    : '(' Expr ')' {SParens $2}
    | UnOp Expr1 {SUnOp $1 $2}
    | Name {SVar $1}
    | Expr1 '[' Expr ']' {SIndex $1 $3}
    | int {SLit (LitInt $1)}
    | true {SLit (LitBool True)}
    | false {SLit (LitBool False)}
    | var '(' ExprList ')' {SCall $1 $3}

lineno :: { AlexPosn }
    : {- empty -}      {% alexGetPosition }

BinOp :: {BinOp}
    : '+' {Plus}
    | '-' {Minus}
    | '*' {Times}
    | '/' {Div}
    | mod {Mod}
    | div {Div}
    | '=' {Eq}
    | '!=' {Neq}
    | '>' {Gt}
    | '>=' {Ge}
    | '<' {Lt}
    | '<=' {Le}
    | '..' {Range}

BinOpRangeConcat :: {BinOp}
    : ',' {RangeConcat}
    | BinOp {$1}

UnOp :: {UnOp}
    : '!' {Not}
    | '-' {Neg}

Solutions :: {Solutions}
    : solutions ':' Solutions1 {$3}
    | {AllSolutions}

Solutions1 :: {Solutions}
    : all {AllSolutions}
    | int {NSolutions $1}

Name :: {(String, AlexPosn)}
    : var lineno {($1, $2)}

{


parseSyntax :: BL.ByteString -> ExceptT String IO [Toplevel]
parseSyntax input = liftEither $ runAlex2 input parseCSP

-- | Generate a nice looking error message based on expected tokens
parseError :: (Token, [String]) -> Parser a
parseError (Token (AlexPn bo line column) t, exps) = do
    inp <- getOrigInput
    let line_str = show line

    fail $ unlines
        [ "Syntax error:"
        -- , "  unexpected '" ++ show t ++ "'"
        , replicate (length line_str) ' ' ++ " |"
        , show line ++ " | " ++ BL8.unpack (findLine inp line)
        , replicate (length line_str) ' ' ++ " | " ++ replicate (column - 1) ' ' ++ "^"
        , case exps of
            []       -> ""
            [s]      -> "  expected " ++ s
            [s2,s1]  -> "  expected " ++ s1 ++ " or " ++ s2
            (s : ss) -> "  expected " ++ (reverse ss >>= (++ ", ")) ++ "or " ++ s
        ]
  where
    findLine :: BL.ByteString -> Int -> BL.ByteString
    findLine inp n = case BL.split 10 inp of
        [] -> ""
        xs -> xs !! (n - 1)

  -- go []     msgs _ = msgs
  -- go (e:es) msgs rs | e `elem` ignore = go es msgs rs
  -- go (e:es) msgs [] = go es (e : msgs) []
  -- go es     msgs ((rep,msg):rs)
  --   | rep `isSubsequenceOf` es = go (es \\ rep) (msg : msgs) rs
  --   | otherwise = go es msgs rs

  -- ignore = words "ntItem ntBlock ntStmt ntPat ntExpr ntTy ntIdent ntPath ntTT\
  --                 ntArm ntImplItem ntTraitItem ntGenerics ntWhereClause ntArg ntLit"

  -- replacements =
  --   [ (words "byte char int float str byteStr rawStr rawByteStr", "a literal"       )
  --   , (words "byte",                                              "a byte"          )
  --   , (words "char",                                              "a character"     )
  --   , (words "int",                                               "an int"          )
  --   , (words "float",                                             "a float"         )
  --   , (words "str",                                               "a string"        )
  --   , (words "byteStr",                                           "a byte string"   )
  --   , (words "rawStr",                                            "a raw string"    )
  --   , (words "rawByteStr",                                        "a raw bytestring")
  --   
  --   , (words "outerDoc innerDoc",                                 "a doc"           )
  --   , (words "outerDoc",                                          "an outer doc"    )
  --   , (words "innerDoc",                                          "an inner doc"    )

  --   , (words "IDENT",                                             "an identifier"   )
  --   , (words "LIFETIME",                                          "a lifetime"      )
  --   ]

}
