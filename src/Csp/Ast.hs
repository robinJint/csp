{-# LANGUAGE DeriveFoldable    #-}
{-# LANGUAGE DeriveFunctor     #-}
{-# LANGUAGE DeriveTraversable #-}

module Csp.Ast where

import           Csp.Lexer (AlexPosn, tokenToPosN)

data AExpr name ty
  = Binop BinOp
          (Info name ty)
          (Info name ty)
  | UnOp UnOp
         (Info name ty)
  | Var name
  | Lit Lit
    -- sugers
    -- TODO remove the list from the first list
  | Forall name
           [Info name ty]
           [Info name ty]
  | Index (Info name ty)
          (Info name ty)
  | Call String
         [Info name ty]
  | Solve String
          [Info name ty]
    -- | Extra extra
  deriving (Show, Eq)

data Info name ty = Info
  { infoLoc :: AlexPosn
  , infoTy  :: ty
  , inner   :: AExpr name ty
  } deriving (Show, Eq)

mapInner :: (AExpr name ty -> AExpr name' ty) -> Info name ty -> Info name' ty
mapInner f info = info {inner = f (inner info)}

mapInnerT ::
     Monad m
  => (AExpr name ty -> m (AExpr name' ty))
  -> Info name ty
  -> m (Info name' ty)
mapInnerT f info = do
  newE <- f (inner info)
  pure info {inner = newE}

-- type StartAST = AExpr String () Suger
-- type NamedAST = AExpr Int () Suger
-- type BareAST = AExpr Int () ()
-- type TypedAST = AExpr Int Type ()
data BinOp
  = Plus
  | Minus
  | Times
  | Div
  | Mod
  | Eq
  | Neq
  | Lt
  | Le
  | Gt
  | Ge
  | Range
  | RangeConcat
  deriving (Show, Eq)

data UnOp
  = Neg
  | Not
  | Abs
  deriving (Show, Eq)

data Lit
  = LitBool Bool
  | LitInt Int
  deriving (Show, Eq)

data DomainSpecifier name expr
  = DomainVar name
  | DomainIndex (DomainSpecifier name expr)
                expr
  deriving (Show, Eq, Functor, Foldable, Traversable)

data Domain name expr
  = Domain [DomainSpecifier name expr]
           [expr]
  | ForallDomain name
                 expr
                 [Domain name expr]
  deriving (Show, Eq, Functor, Foldable, Traversable)

data Solutions
  = NSolutions Int
  | AllSolutions
  deriving (Show, Eq)

data Loc a =
  Loc AlexPosn
      a

data Ast name expr = Problem
  { name        :: (String, AlexPosn)
  , input       :: [([name], Type)]
  , output      :: [([name], Type)]
  , variables   :: [([name], Type)]
  , domains     :: [Domain name expr]
  , constraints :: [expr]
  , solutions   :: Solutions
  } deriving (Show, Eq, Functor, Foldable, Traversable)

type TypedAst = Ast Name (Info Name Type)

type UnifiedAst = Ast UnifyName (Info UnifyName Type)

data Type
  = Type String
  | TypeArray Type
              Int
  | Any
  deriving (Show, Eq)

data Name = Name
  { originalName :: String
  , nameId       :: Int
  , nameLoc      :: AlexPosn
  } deriving (Show)

instance Eq Name where
  Name {nameId = a} == Name {nameId = b} = a == b

instance Ord Name where
  compare Name {nameId = a} Name {nameId = b} = compare a b

data UnifyName = UnifyName
  { baseName  :: Name
  , functionN :: Int
  } deriving (Show, Eq, Ord)
