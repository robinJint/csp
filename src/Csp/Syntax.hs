module Csp.Syntax where

import           Csp.Ast   as AST
import           Csp.Lexer (AlexPosn)

type Toplevel = Ast (String, AlexPosn) Expr

type SDomain = AST.Domain (String, AlexPosn) Expr

type SDomainSpecifier = AST.DomainSpecifier (String, AlexPosn) Expr

data Expr
  = BinOp AlexPosn
          BinOp
          AlexPosn
          Expr1
          Expr
  | Expr1 AlexPosn
          Expr1
  deriving (Show, Eq)

data Expr1
  = SUnOp UnOp
          Expr1
  | SLit Lit
  | SVar (String, AlexPosn)
  | SIndex Expr1
           Expr
  | SParens Expr
  | SCall String
          [Expr]
  | SSolve String
           [Expr]
  | SForall (String, AlexPosn)
            [Expr]
            [Expr]
  deriving (Show, Eq)
