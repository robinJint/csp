{

{-# LANGUAGE OverloadedStrings                 #-}
{-# LANGUAGE NoMonomorphismRestriction          #-}
{-# LANGUAGE CPP                                #-}
{-# OPTIONS_GHC -fno-warn-unused-binds          #-}
{-# OPTIONS_GHC -fno-warn-missing-signatures    #-}
{-# OPTIONS_GHC -fno-warn-unused-matches        #-}
{-# OPTIONS_GHC -fno-warn-unused-imports        #-}
{-# OPTIONS_GHC -fno-warn-name-shadowing        #-}
{-# OPTIONS_GHC -fno-warn-tabs                  #-}
{-# OPTIONS_GHC -funbox-strict-fields           #-}

module Csp.Lexer
  ( Alex(..)
  , AlexPosn(..)
  , AlexState(..)
  , Token(..)
  , TokenClass(..)
  , alexError
  , alexMonadScan
  , runAlex
  , tokenToPosN
  , alexGetInput
  , alexStartPos
  , getOrigInput
  )
where

import System.Exit
import qualified Data.ByteString.Lazy.Char8 as B
}

%wrapper "monadUserState-bytestring"

$digit = 0-9                    -- digits
$alpha = [a-zA-Z]               -- alphabetic characters

tokens :-

  $white+                               ;
  "--".*                                ;
  "#".*                                ;
  let                                   { tok          TokenLet }
  begin                                   { tok          TokenBegin }
  end                                   { tok          TokenEnd }
  csp                                   { tok          TokenCsp }
  input                                   { tok          TokenInput }
  output                                   { tok          TokenOutput }
  variables                                   { tok          TokenVariables }
  domains                                   { tok          TokenDomains }
  constraints                                   { tok          TokenConstraints }
  solutions                                   { tok          TokenSolutions }
  solve                                   { tok          TokenSolve }
  forall                                   { tok          TokenForall }
  in                                    { tok          TokenIn }
  True                                    { tok          TokenTrue }
  False                                    { tok          TokenFalse }
  all                                    { tok          TokenAll }
  div                                    { tok          TokenDiv }
  mod                                    { tok          TokenMod }
  $digit+                               { tok_read     TokenInt }
  "+"                                  { tok          TokenPlus }
  "-"                                  { tok          TokenMinus }
  "*"                                  { tok          TokenTimes }
  "/"                                  { tok          TokenDiv }
  "^"                                  { tok          TokenPow }
  "="                                  { tok          TokenEQ }
  "<>"                                   { tok          TokenNEQ }
  "!="                                   { tok          TokenNEQ }
  "!"                                   { tok          TokenNOT }
  ">"                                   { tok          TokenGT }
  ">="                                   { tok          TokenGE }
  "<"                                   { tok          TokenLT }
  "<="                                   { tok          TokenLE }
  "("                                  { tok          TokenOB }
  ")"                                  { tok          TokenCB }
  "["                                  { tok          TokenOSB }
  "]"                                  { tok          TokenCSB }
  ":"                                  { tok          TokenColon }
  ";"                                  { tok          TokenSemiColon }
  "<-"                                  { tok          TokenArrowLeft }
  ","                                  { tok          TokenComma }
  ".."                                  { tok          TokenDoubleDot }
  $alpha [$alpha $digit \_ \']*         { tok_string   TokenVar }
{
-- Some action helpers:
tok' f (p, _, input, _) len = return $ Token p (f (B.take (fromIntegral len) input))
tok x = tok' (\s -> x)
tok_string x = tok' (\s -> x (B.unpack s))
tok_read x = tok' (\s -> x (read (B.unpack s)))

-- The token type:
data Token = Token AlexPosn TokenClass
  deriving (Show)

tokenToPosN :: Token -> AlexPosn
tokenToPosN (Token p _) = p

data TokenClass
 = TokenLet
 | TokenCsp
 | TokenBegin
 | TokenEnd
 | TokenIn
 | TokenInput
 | TokenOutput
 | TokenVariables
 | TokenDomains
 | TokenConstraints
 | TokenSolutions
 | TokenSolve
 | TokenInt    Int
 | TokenVar    String
 | TokenTrue
 | TokenFalse
 | TokenAll
 | TokenPlus
 | TokenMinus
 | TokenTimes
 | TokenDiv
 | TokenMod
 | TokenPow
 | TokenEQ
 | TokenNEQ
 | TokenNOT
 | TokenGT
 | TokenGE
 | TokenLT
 | TokenLE
 | TokenOB
 | TokenCB
 | TokenOSB
 | TokenCSB
 | TokenColon
 | TokenSemiColon
 | TokenEOF
 | TokenComma
 | TokenArrowLeft
 | TokenDoubleDot
 | TokenForall
 deriving (Eq, Show)

alexEOF :: Alex Token
alexEOF = do
  (p, _, _, _) <- alexGetInput
  return $ Token p TokenEOF

type AlexUserState = B.ByteString
alexInitUserState = undefined

getOrigInput :: Alex B.ByteString
getOrigInput = Alex $ \s@AlexState{alex_ust=ust} -> Right (s, ust)

}
