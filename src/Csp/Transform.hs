{-# LANGUAGE BlockArguments   #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE NamedFieldPuns   #-}
{-# LANGUAGE RecordWildCards  #-}
{-# LANGUAGE TupleSections    #-}

module Csp.Transform where

import           Control.Monad.Except
import qualified Control.Monad.State.Strict as S
import           Data.Functor.Identity
import qualified Data.Map                   as Map
import qualified Data.List                  as List
import           Data.Maybe                 (catMaybes, fromJust, isJust,
                                             mapMaybe, fromMaybe)

import           Csp.Ast
import           Csp.Error
import           Csp.Lexer                  (AlexPosn (..))
import qualified Csp.Syntax                 as Syntax

syntaxToAst ::
     Syntax.Toplevel
  -> ExceptT [CspError] IO (Ast (String, AlexPosn) (Info (String, AlexPosn) ()))
syntaxToAst = pure . fmap shuntingyard

precedence :: BinOp -> Int
precedence o =
  case o of
    Plus        -> 2
    Minus       -> 2
    Times       -> 3
    Div         -> 3
    Mod         -> 3
    Eq          -> 1
    Neq         -> 1
    Lt          -> 1
    Le          -> 1
    Gt          -> 1
    Ge          -> 1
    Range       -> 0
    RangeConcat -> -1

shuntingyard :: Syntax.Expr -> Info (String, AlexPosn) ()
shuntingyard e = go e [] []
  where
    ispush (x:xs) y = precedence y > precedence x
    ispush [] _     = True
    go ::
         Syntax.Expr
      -> [(AlexPosn, BinOp)]
      -> [Info (String, AlexPosn) ()]
      -> Info (String, AlexPosn) ()
    go (Syntax.BinOp loc op e1loc e1 rest) oprStack valueStack =
      let (newOprStack, newValueStack) =
            handleStack (loc, op) oprStack (expr1ToAExpr e1loc e1 : valueStack)
       in go rest newOprStack newValueStack
    go (Syntax.Expr1 loc e1) oprs vals =
      finish oprs (expr1ToAExpr loc e1 : vals)
    handleStack ::
         (AlexPosn, BinOp)
      -> [(AlexPosn, BinOp)]
      -> [Info (String, AlexPosn) ()]
      -> ([(AlexPosn, BinOp)], [Info (String, AlexPosn) ()])
    handleStack (startLoc, newOp) oprStack valueStack
      | ispush (map snd oprStack) newOp =
        ((startLoc, newOp) : oprStack, valueStack)
      | otherwise =
        let (a:b:restValue) = valueStack
            ((loc, o):restOpr) = oprStack
         in handleStack
              (startLoc, newOp)
              restOpr
              ((Info loc () $ Binop o b a) : restValue)
    finish ::
         [(AlexPosn, BinOp)]
      -> [Info (String, AlexPosn) ()]
      -> Info (String, AlexPosn) ()
    finish ((loc, op):ops) (valA:valB:vals) =
      finish ops (Info loc () (Binop op valB valA) : vals)
    finish [] [val] = val
    finish a b = error $ (show a) ++ (show b)

expr1ToAExpr :: AlexPosn -> Syntax.Expr1 -> Info (String, AlexPosn) ()
expr1ToAExpr loc e1 =
  case e1 of
    Syntax.SUnOp op e -> Info loc () $ UnOp op (expr1ToAExpr loc e)
    Syntax.SLit l -> Info loc () $ Lit l
    Syntax.SVar v -> Info loc () $ Var v
    Syntax.SIndex b arg ->
      Info loc () $ Index (expr1ToAExpr loc b) (shuntingyard arg)
    Syntax.SParens e -> shuntingyard e
    Syntax.SForall v ds body ->
      Info loc () $ Forall v (map shuntingyard ds) (map shuntingyard body)
    Syntax.SCall f args -> Info loc () $ Call f $ map shuntingyard args
    Syntax.SSolve f args -> Info loc () $ Solve f $ map shuntingyard args
    -- | TODO
    -- Syntax.SCall f args ->
    -- Syntax.SForall String [Expr] [Expr] ->

nameAst ::
     Ast (String, AlexPosn) (Info (String, AlexPosn) ty)
  -> ExceptT [CspError] IO (Ast Name (Info Name ty))
nameAst = pure . naming

fresh :: S.MonadState Int m => m Int
fresh = do
  i <- S.get
  S.modify (+ 1)
  pure i

mapDomain ::
     Monad m
  => (name -> m name')
  -> (name -> m name')
  -> Domain name (Info name ty)
  -> m (Domain name' (Info name' ty))
mapDomain f fCreate d =
  case d of
    Domain ds es -> do
      ds' <- mapM (mapDomainSpecifier f fCreate) ds
      es' <- mapM (mapExpr f fCreate) es
      pure (Domain ds' es')
    ForallDomain name expr inner -> do
      name' <- fCreate name
      expr' <- mapExpr f fCreate expr
      inner' <- mapM (mapDomain f fCreate) inner
      pure (ForallDomain name' expr' inner')

mapDomainSpecifier ::
     (Monad m)
  => (name -> m name')
  -> (name -> m name')
  -> DomainSpecifier name (Info name ty)
  -> m (DomainSpecifier name' (Info name' ty))
mapDomainSpecifier f fCreate ds =
  case ds of
    DomainVar n -> DomainVar <$> f n
    DomainIndex inner expr -> do
      inner' <- mapDomainSpecifier f fCreate inner
      expr' <- mapExpr f fCreate expr
      pure (DomainIndex inner' expr')

mapExpr ::
     (Monad m)
  => (name -> m name')
  -> (name -> m name')
  -> Info name ty
  -> m (Info name' ty)
mapExpr f g info = mapExprToExpr (\x -> Var <$> f x) g info

mapExprToExpr ::
     (Monad m)
  => (name -> m (AExpr name' ty))
  -> (name -> m name')
  -> Info name ty
  -> m (Info name' ty)
mapExprToExpr f createF info =
  let loc = infoLoc info
   in flip mapInnerT info $ \expr ->
        case expr of
          Binop op a b -> do
            a' <- mapExprToExpr f createF a
            b' <- mapExprToExpr f createF b
            pure $ Binop op a' b'
          UnOp uop e -> do
            e' <- mapExprToExpr f createF e
            pure $ UnOp uop e'
          Var n -> f n
          Lit l -> pure $ Lit l
          Index base arg -> do
            base' <- mapExprToExpr f createF base
            arg' <- mapExprToExpr f createF arg
            pure $ Index base' arg'
          Call callf args -> do
            args' <- mapM (mapExprToExpr f createF) args
            pure $ Call callf args'
          Solve callf args -> do
            args' <- mapM (mapExprToExpr f createF) args
            pure $ Solve callf args'
          Forall n values body -> do
            values' <- mapM (mapExprToExpr f createF) values
            newN <- createF n
            body' <- mapM (mapExprToExpr f createF) body
            pure $ Forall newN values' body'

-- namingF :: (Monad m, MonadError [CspError] m) => (gn -> m gn') -> (en -> m en') -> Ast gn (Info en ty) -> Ast gn' (Info en' ty)
naming ::
     Ast (String, AlexPosn) (Info (String, AlexPosn) ty)
  -> Ast Name (Info Name ty)
naming ast =
  flip S.evalState 0 $ do
    nameEnv <- baseNameEnv ast
    domains' <- mapM (convertDomain nameEnv) (domains ast)
    -- constraints' <- mapM (mapExpr nameEnv) (constraints ast)
    let fExpr (n, loc) = S.gets (getWith n)
        getWith :: (Show k, Ord k) => k -> Map.Map k v -> v
        getWith n m = maybe (error $ show n ++ "not found") id $ Map.lookup n m
        fExprCreate (n, loc) = do
          id <- lift fresh
          let new = Name n id loc
          S.modify $ Map.insert n new
          pure new
    constraints' <-
      flip S.evalStateT nameEnv $
      mapM (mapExpr fExpr fExprCreate) (constraints ast)
    pure
      Problem
        { name = name ast
        , input =
            map
              (\(ns, t) ->
                 (map (fromJust . flip Map.lookup nameEnv . fst) ns, t)) $
            input ast
        , output =
            map
              (\(ns, t) ->
                 (map (fromJust . flip Map.lookup nameEnv . fst) ns, t)) $
            output ast
        , variables =
            map
              (\(ns, t) ->
                 (map (fromJust . flip Map.lookup nameEnv . fst) ns, t)) $
            variables ast
        , constraints = constraints'
        , domains = domains'
        , solutions = solutions ast
        }
  where
    baseNameEnv ::
         Ast (String, AlexPosn) (Info (String, AlexPosn) ty)
      -> S.State Int (Map.Map String Name)
    baseNameEnv Problem {..} = Map.fromList . concat <$> giveNames names
      where
        names :: [[(String, AlexPosn)]]
        names =
          [ concat (map fst input)
          , concat (map fst output)
          , concat (map fst variables)
          ]
    giveNames = mapM (mapM g)
      where
        g :: (String, AlexPosn) -> S.State Int (String, Name)
        g (n, loc) = (,) n <$> mkName n loc
                    -- i <- S.get
                    -- S.put (i + 1)
                    -- pure (n, Name n i)
    mkName :: String -> AlexPosn -> S.State Int Name
    mkName n loc = do
      i <- fresh
      pure (Name n i loc)
    convertDomain ::
         Map.Map String Name
      -> Domain (String, AlexPosn) (Info (String, AlexPosn) ty)
      -> S.State Int (Domain Name (Info Name ty))
    convertDomain env d =
      case d of
        Domain ds es -> do
          ds' <- mapM (convertDomainSpecifier env) ds
          es' <- mapM (go env) es
          pure (Domain ds' es')
        ForallDomain (name, loc) expr inner -> do
          name' <- mkName name loc
          expr' <- go env expr
          inner' <- mapM (convertDomain (Map.insert name name' env)) inner
          pure (ForallDomain name' expr' inner')
    convertDomainSpecifier ::
         Map.Map String Name
      -> DomainSpecifier (String, AlexPosn) (Info (String, AlexPosn) ty)
      -> S.State Int (DomainSpecifier Name (Info Name ty))
    convertDomainSpecifier env ds =
      flip S.evalStateT env $
      mapDomainSpecifier
        (pure . fromJust . flip Map.lookup env . fst)
        (\(n, loc) -> do
           id <- lift fresh
           let name = Name n id loc
           S.modify $ Map.insert n name
           pure name)
        ds
    go ::
         Map.Map String Name
      -> Info (String, AlexPosn) ty
      -> S.State Int (Info Name ty)
    go env info =
      flip S.evalStateT env $
      mapExpr
        (\(n, loc) -> S.gets $ \s -> fromJust $ Map.lookup n s)
        (\(n, loc) -> do
           id <- lift fresh
           let name = Name n id loc
           S.modify $ Map.insert n name
           pure name)
        info

liftMaybe :: MonadError e m => e -> Maybe a -> m a
liftMaybe err = maybe (throwError err) pure

-- | simplify and combine all the Ast's
unify :: [TypedAst] -> ExceptT [CspError] IO TypedAst
unify xs = do
  let genv = Map.fromList . map (\x -> (fst . name $ x, x)) $ xs
  mainCsp <-
    liftMaybe [otherErr "cannot find 'main' csp"] $
    addFCode 0 <$> Map.lookup "main" genv
  let getSolve (Solve n a) = Just (n, a)
      getSolve _           = Nothing
      isSolve :: AExpr n ty -> Bool
      isSolve = isJust . getSolve
      solves = mapMaybe (getSolve . inner) $ constraints mainCsp
      mainWithoutSolves =
        mainCsp
          {constraints = filter (not . isSolve . inner) (constraints mainCsp)}
      f :: S.MonadState Int m => m UnifiedAst -> (TypedAst, [Info UnifyName Type]) -> m UnifiedAst
      f a new = do
        a' <- a
        inline a' new
  solvesAst <-
    mapM
      (liftMaybe [otherErr "could not find csp"] . \(n, a) -> (,a) <$> (Map.lookup n genv))
      solves
  let unified = flip S.evalState 1 $ foldl f (pure mainWithoutSolves) solvesAst
      renamed = renameAst unified
  pure renamed

unifyBase :: Applicative m => m UnifiedAst
unifyBase =
  pure
    Problem
      { name = ("", AlexPn 0 0 0)
      , input = []
      , output = []
      , variables = []
      , domains = []
      , constraints = []
      , solutions = AllSolutions
      }

renameAst :: UnifiedAst -> TypedAst
renameAst ast =
  flip S.evalState 0 $
  flip S.evalStateT Map.empty $
  mapAst
    (\un -> S.gets (fromJust . Map.lookup un))
    (\un -> do
       let UnifyName (Name n _ loc) _ = un
       i <- lift fresh
       let name = Name n i loc
       S.modify (Map.insert un name)
       pure name)
    ast

mapAst ::
     (Monad m)
  => (name -> m name')
  -> (name -> m name')
  -> Ast name (Info name ty)
  -> m (Ast name' (Info name' ty))
mapAst find create ast = do
  let doublemapfstM :: Monad m => (a -> m a') -> ([a], b) -> m ([a'], b)
      doublemapfstM f (x, y) = mapM f x >>= pure . (, y)
  newInput <- mapM (doublemapfstM create) $ input ast
  newOutput <- mapM (doublemapfstM create) $ output ast
  newVariables <- mapM (doublemapfstM create) $ variables ast
  newDomains <- mapM (mapDomain find create) $ domains ast
  newConstraints <- mapM (mapExpr find create) $ constraints ast
  pure
    Problem
      { name = name ast
      , input = newInput
      , output = newOutput
      , variables = newVariables
      , domains = newDomains
      , constraints = newConstraints
      , solutions = solutions ast
      }

addFCode :: Int -> TypedAst -> UnifiedAst
addFCode i ast =
  let convertName n = UnifyName n i
      convertNameM = pure . convertName
      newInput = map (\(ns, ty) -> (map convertName ns, ty)) $ input ast
      newOutput = map (\(ns, ty) -> (map convertName ns, ty)) $ output ast
      newVariables = map (\(ns, ty) -> (map convertName ns, ty)) $ variables ast
      newDomains =
        map (runIdentity . mapDomain convertNameM convertNameM) $ domains ast
      newConstraints =
        map (runIdentity . mapExpr convertNameM convertNameM) $ constraints ast
   in Problem
        { name = name ast
        , input = newInput
        , output = newOutput
        , variables = newVariables
        , domains = newDomains
        , constraints = newConstraints
        , solutions = solutions ast
        }

replaceName old new expr = 
  flip S.evalState (Map.singleton old new) $ mapExpr
    (\n -> S.gets (fromMaybe n . Map.lookup n))
    (\n -> S.modify (Map.delete n))
    expr

findDomain :: Eq name => name -> [Domain name expr] -> expr
findDomain name ds = fromJust $ List.lookup name $ concatDomains ds

concatDomains :: [Domain name expr] -> [(name, expr)]
concatDomains = map f
  where
    f (Domain [DomainVar name] [expr]) = (name, expr)

inline :: (S.MonadState Int m) => UnifiedAst -> (TypedAst, [Info UnifyName Type]) -> m UnifiedAst
inline base (new, args) = do
  newFCode <- fresh
  let new' = addFCode newFCode new
      pos = AlexPn 0 0 0
      equal a b = Info pos (Type "boolean") (Binop Eq a b)

      -- TODO use this concat in the typechecker
      inputArgs = concat $ map (\(ns, ty) -> (map (\n -> Info pos ty (Var n)) ns)) $ input new'
      argsConstraints = map (uncurry equal) $ zip args inputArgs

  pure
    base
      { variables = concat [variables base, variables new', input new', output new']
      , domains = domains base ++ domains new' 
      , constraints = constraints base ++ constraints new' ++ argsConstraints
      }
