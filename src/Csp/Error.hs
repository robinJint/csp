{-# LANGUAGE RecordWildCards #-}

module Csp.Error where

import qualified Data.ByteString.Lazy       as BL
import qualified Data.ByteString.Lazy.Char8 as BL8

import           Csp.Ast                    (Name, Type)
import           Csp.Lexer                  (AlexPosn (..))

data CspError = CspError
  { errKind    :: CspErrorKind
  , errMessage :: String
  , errLoc     :: Maybe AlexPosn
  } deriving (Show, Eq)

data CspErrorKind
  = ParseError
  | TypeError Type
              Type
  | IndexError Type
  | NameError String
  | DuplicateName String
                  AlexPosn
  | InvalidArgN String
                Int
                Int
  | Other String
  deriving (Show, Eq)

otherErr :: String -> CspError
otherErr msg = CspError (Other msg) "" Nothing

prettyErr :: BL.ByteString -> String -> CspError -> String
prettyErr source filename CspError {..} =
  unlines $
  [ "error[]: " ++ prettyErrorKind errKind
  , " --> " ++ filename ++ ":" ++ maybe "" prettyLoc errLoc
  ] ++
  sourceLine ++ [errMessage, ""]
  where
    findLine :: BL.ByteString -> Int -> BL.ByteString
    findLine inp n =
      case BL.split 10 inp of
        [] -> BL.empty
        xs -> xs !! (n - 1)
    prettyLoc :: AlexPosn -> String
    prettyLoc (AlexPn _ l c) = show l ++ ":" ++ show c
    prettyErrorKind :: CspErrorKind -> String
    prettyErrorKind ek =
      case ek of
        ParseError -> "parse error"
        TypeError x y ->
          "type error, could match type " ++ show x ++ " with type " ++ show y
        IndexError t -> "index error"
        NameError str -> "name error"
        DuplicateName a aloc -> "duplicate name '" ++ a ++ "'"
        InvalidArgN name expected actual ->
          "'" ++
          name ++
          "' expects " ++
          show expected ++ " but received " ++ show actual ++ " arguments"
        Other msg -> msg
    sourceLine :: [String]
    sourceLine =
      case errLoc of
        Nothing -> []
        Just (AlexPn _ linen coln) ->
          let lineNumberStr = show $ linen
              lineNLen = length lineNumberStr
           in [ replicate lineNLen ' ' ++ " |"
              , lineNumberStr ++ " | " ++ BL8.unpack (findLine source linen)
              , replicate lineNLen ' ' ++
                " | " ++ replicate (coln - 2) ' ' ++ "^"
              ]
