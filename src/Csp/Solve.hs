{-# LANGUAGE MultiParamTypeClasses, FlexibleInstances, NamedFieldPuns, LambdaCase, TupleSections #-}

module Csp.Solve where

import Csp.Ast
import Csp.Error
import Csp.Transform (mapAst)

import qualified Data.Map.Strict       as M
import qualified Data.Set as Set
import Data.Set (Set)
import           Control.Monad.Except
import Data.RangeSet.List (RSet)
import Data.Functor.Identity
import qualified Data.RangeSet.List as RSet
import qualified Control.Monad.State.Strict as S
import Data.Maybe (catMaybes, fromJust, mapMaybe)
import Debug.Trace

type Expr = Info Int Type

solveAst :: TypedAst -> IO (Maybe (M.Map Int Int))
solveAst ast = pure $ flip S.evalStateT Err $ backtrack vars M.empty (constraints intAst)
  where
    intAst :: Ast Int (Info Int Type)
    intAst = runIdentity $ mapAst (pure . nameId) (pure . nameId) ast
    vars = foldl M.union M.empty $ map domainToVar $ domains intAst

domainToVar :: Domain Int Expr -> Vars
domainToVar (Domain ds expr) = vars
  where
    ranges = (foldl RSet.union RSet.empty $ map (getRange . runIdentity . eval (error "no vars allowed in domains")) expr)
    vars = M.fromList $ map ((,ranges) . domainSpecifierToVar) ds

domainSpecifierToVar :: DomainSpecifier Int Expr -> Int
domainSpecifierToVar (DomainVar n) = n
domainSpecifierToVar _ = error "domain index left in solve"

data Val = 
    IntVal Int
  | BoolVal Bool
  | RangeVal (RSet Int)
  deriving (Eq, Ord, Show)

getInt (IntVal i) = i
getBool (BoolVal b) = b
getRange (RangeVal r) = r

isTrue (BoolVal True) = True
isTrue _ = False

-- binopInt l x y f = f (getInt $ eval l x) (getInt $ eval l y)
binopInt l x y f = f <$> (getInt <$> eval l x) <*> (getInt <$> eval l y)
binopBool l x y f = f <$> (getBool <$> eval l x) <*> (getBool <$> eval l y)
binopRange l x y f = f <$> (getRange <$> eval l x) <*> (getRange <$> eval l y)

eval :: Monad m => (Int -> m Val) -> Expr -> m Val
-- eval l info | trace ("eval: " ++ show (inner info)) False = undefined
eval lookup Info{inner} = case inner of
  Lit (LitInt i) -> pure $ IntVal i
  Lit (LitBool b) -> pure $ BoolVal b

  Var name -> lookup name

  UnOp Neg body -> IntVal . negate . getInt <$> eval lookup body
  UnOp Not body -> BoolVal . not . getBool <$> eval lookup body
  UnOp Abs body -> IntVal . abs . getInt <$> eval lookup body

  Binop Plus x y -> IntVal <$> binopInt lookup x y (+)
  Binop Minus x y -> IntVal <$> binopInt lookup x y (-)
  Binop Times x y -> IntVal <$> binopInt lookup x y (*)
  Binop Div x y -> IntVal <$> binopInt lookup x y div
  Binop Mod x y -> IntVal <$> binopInt lookup x y mod

  Binop Eq x y -> do
    x' <- eval lookup x 
    y' <- eval lookup y
    pure . BoolVal $ x' == y'
  Binop Neq x y -> do
    x' <- eval lookup x 
    y' <- eval lookup y
    pure . BoolVal $ x' /= y'
  Binop Lt x y -> BoolVal <$> binopInt lookup x y (<)
  Binop Le x y -> BoolVal <$> binopInt lookup x y (<=)
  Binop Gt x y -> BoolVal <$> binopInt lookup x y (>)
  Binop Ge x y -> BoolVal <$> binopInt lookup x y (>=)
  Binop Range x y -> RangeVal <$> binopInt lookup x y (curry RSet.singletonRange)
  Binop RangeConcat x y -> do
    let toRange = \case 
          IntVal i -> RSet.singleton i
          RangeVal r -> r
    y' <- toRange <$> eval lookup y
    x' <- toRange <$> eval lookup x
    pure . RangeVal $ RSet.union x' y'

type Vars = M.Map Int (RSet Int)
type Assign = M.Map Int Int

backtrack :: Solver s => Vars -> Assign -> [Expr] -> S.StateT s Maybe (M.Map Int Int)
-- backtrack vars assign constr | trace ("backtrack " ++ show vars ++ ", " ++ show assign) False = undefined
backtrack vars assign constr =
  if M.size vars == M.size assign
    then pure assign
    else do
      (var, domain) <- selectVar vars assign
      orderedDomain <- orderDomain domain

      state <- S.get
      S.lift . safeHead . catMaybes . flip map orderedDomain $ \val -> flip S.evalStateT state $ do
        consistent <- isConsistent var val assign constr
        unless consistent quit

        let assign' = M.insert var val assign
        inferences <- lift =<< inference vars assign'
        vars' <- lift $ addInferences vars inferences

        backtrack vars' assign' constr

safeHead (x:_) = Just x
safeHead [] = Nothing

quit :: S.StateT a Maybe b
quit = lift Nothing

class Solver a where
  selectVar :: S.MonadState a m => Vars -> Assign -> m (Int, RSet Int)
  orderDomain :: S.MonadState a m => RSet Int -> m [Int]
  isConsistent :: S.MonadState a m => Int -> Int -> Assign -> [Expr] -> m Bool
  inference :: S.MonadState a m => Vars -> Assign -> m (Maybe Vars)

data Err = Err

-- showTrace msg x = trace (msg ++ ": " ++ show x) x
showTrace msg x = x

instance Solver Err where
  selectVar vars assign = pure . showTrace "selectVar" $ (id, fromJust $ M.lookup id vars)
    where id = head $ Set.toList $ M.keysSet vars `Set.difference` M.keysSet assign

  orderDomain = pure . RSet.toList

  isConsistent var val assign constr = pure . showTrace "isConsis" $ res
    where 
      assign' = M.insert var val assign
      lookup = showTrace "lookup" . (IntVal <$>) . flip M.lookup assign'
      res = all isTrue . mapMaybe (eval lookup) $ constr

  inference _ _ = pure $ Just M.empty

addInferences :: Vars -> Vars -> Maybe Vars
addInferences x y = Just $ M.union x y

