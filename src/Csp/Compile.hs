{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE RecordWildCards   #-}

module Csp.Compile where

import           Control.Monad.Except
import           Data.List

import           Csp.Ast
import           Csp.Error

compileAst :: Ast Name (Info Name ty) -> String
compileAst = compile

class Compile a where
  compile :: a -> String

instance (Compile e, Compile n) => Compile (Ast n e) where
  compile Problem {..} =
    unlines
      [ "variables:"
      , concat $ map ((++ ";\n") . compileVars) variables
      , "domains:"
      , concat $ map ((++ ";\n") . compile) domains
      , "constraints:"
      , concat $ map ((++ ";\n") . compile) constraints
      , "solutions:"
      , compile solutions
      ]

instance (Compile n) => Compile (AExpr n a) where
  compile e =
    case e of
      Binop op a b ->
        unwords [params (compile a), compile op, params (compile b)]
      UnOp uop e -> unwords [compile uop, compile e]
      Var n -> compile n
      Lit l -> compile l
      Forall n vals expr ->
        unwords $ [compile n] ++ map compile vals ++ map compile expr
      Index base arg -> unwords [compile base, "[", compile arg, "]"]
      Call s args -> error "call"
      Solve s args -> error "solve"
    where
      params x = unwords ["(", x, ")"]

instance (Compile n) => Compile (Info n a) where
  compile = compile . inner

instance Compile BinOp where
  compile e =
    case e of
      Plus  -> "+"
      Minus -> "-"
      Times -> "*"
      Div   -> "div"
      Mod   -> "mod"
      Eq    -> "="
      Neq   -> "<>"
      Lt    -> "<"
      Le    -> "<="
      Gt    -> ">"
      Ge    -> ">="
      Range -> ".."

instance Compile UnOp where
  compile e =
    case e of
      Neg -> "-"
      Not -> "!"

instance Compile Lit where
  compile e =
    case e of
      LitBool b -> show b
      LitInt i  -> show i

instance (Compile a, Compile n) => Compile (Domain n a) where
  compile e =
    case e of
      Domain ns ranges ->
        unwords
          [ intercalate "," (map compile ns)
          , "<-"
          , unwords ["[", intercalate "," (map compile ranges), "]"]
          ]

instance (Compile a) => Compile (DomainSpecifier a b) where
  compile e =
    case e of
      DomainVar n -> compile n

instance Compile Solutions where
  compile e =
    case e of
      NSolutions n -> show n
      AllSolutions -> "all"

instance Compile Name where
  compile (Name n i l) = "var_" ++ show i ++ "_" ++ n

instance Compile Type where
  compile e =
    case e of
      Type n         -> n
      TypeArray ty i -> concat [compile ty, "[", show i, "]"]

compileVars :: (Compile a) => ([a], Type) -> String
compileVars (ns, ty) =
  unwords [intercalate "," (map compile ns), ":", compile ty]
