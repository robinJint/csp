{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE NamedFieldPuns        #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE RecordWildCards       #-}

module Csp.Typecheck where

import           Control.Monad.Except
import           Control.Monad.Writer
import qualified Data.Map.Merge.Strict as M
import qualified Data.Map.Strict       as M
import           Data.Maybe            (fromJust)

import           Csp.Ast               as Ast
import           Csp.Error
import           Csp.Lexer             as AlexPosn

type LEnv = M.Map Name Type

type TGEnv = M.Map String ([(String, Type)], [(String, Type)])

type TypeT = ExceptT [CspError] IO

type TypeTW = WriterT [CspError] TypeT

type TypedAST = Ast Name (Info Name Type)

-- | Union two maps, applying some effectful function to duplicates.
unionWithA ::
     (Applicative f, Ord k)
  => (k -> a -> a -> f a)
  -> M.Map k a
  -> M.Map k a
  -> f (M.Map k a)
unionWithA f m1 m2 =
  M.mergeA
    M.preserveMissing -- Preserve keys found in m1 but not m2
    M.preserveMissing -- Preserve keys found in m2 but not m1
    (M.zipWithAMatched f) -- Apply f when keys in both m1 and m2
    m1
    m2

runError ::
     (Monad m)
  => WriterT [CspError] (ExceptT [CspError] m) a
  -> ExceptT [CspError] m a
runError f = do
  (res, msgs) <- runWriterT f
  case msgs of
    [] -> pure res
    e  -> throwError $ e

typecheckAst ::
     Show ty
  => [Ast Name (Info Name ty)]
  -> ExceptT [CspError] IO [Ast Name (Info Name Type)]
typecheckAst ps = do
  genv <- runError astMap
  mapM (astCheck genv) ps
  where
    astList = map (\p -> M.singleton (fst $ name p) p) ps
    astMap :: (Monad a) => WriterT [CspError] a (M.Map String _)
    astMap = foldl (\x y -> x >>= tryMerge y) (pure M.empty) astList
    tryMerge ::
         Monad a
      => M.Map String (Ast n e)
      -> M.Map String (Ast n e)
      -> WriterT [CspError] a (M.Map String (Ast n e))
    tryMerge a b = do
      tell $ map dupErr $ M.elems $ M.intersection a b
      pure $ M.union a b
    dupErr Problem {name} =
      CspError (DuplicateName (fst name) (AlexPn 0 0 0)) "" (Just (snd name))

astCheck ::
     Show ty
  => _
  -> Ast Name (Info Name ty)
  -> TypeT (Ast Name (Info Name Type))
astCheck genv ast =
  runError $ do
    lenv <- localTEnv
    checkV genv lenv $ variables ast
    d' <- checkD genv lenv $ domains ast
    c' <- checkC genv lenv $ constraints ast
    pure
      Problem
        { name = name ast
        , input = input ast
        , output = output ast
        , variables = variables ast
        , domains = d'
        , constraints = c'
        , solutions = solutions ast
        }
        -- localTEnv = undefined
  where
    localTEnv :: TypeTW (M.Map Name Type)
    localTEnv = do
      let allNames =
            concat $
            map (\(ns, t) -> [M.singleton (n) t | n <- ns]) $
            input ast ++ output ast ++ variables ast
      tell $
        map
          (\Name {originalName, nameLoc} ->
             CspError (DuplicateName originalName nameLoc) "" (Just nameLoc)) $
        M.keys $ foldl M.intersection M.empty allNames
      pure $ foldl M.union M.empty allNames
        -- | TODO check that all types are int or
    checkV genv lenv _ = pure ()
    checkD ::
         Show a
      => _
      -> _
      -> [Domain Name (Info Name a)]
      -> TypeTW [Domain Name (Info Name Type)]
    checkD genv lenv =
      mapM $ \d ->
        case d of
          Domain sps ds -> do
            sps' <- mapM (checkDS genv lenv) sps
            ds' <- mapM (checkE genv lenv) ds
            pure $ Domain sps' ds'
          ForallDomain n val body -> do
            val' <- checkE genv lenv val
            let lenv' = M.insert n (infoTy val') lenv
            body' <- checkD genv lenv' body
            pure $ ForallDomain n val' body'
    checkDS ::
         Show a
      => _
      -> _
      -> DomainSpecifier Name (Info Name a)
      -> TypeTW (DomainSpecifier Name (Info Name Type))
    checkDS genv lenv ds =
      case ds of
        DomainVar n -> pure $ DomainVar n
        DomainIndex body arg -> do
          body' <- checkDS genv lenv body
          arg' <- checkE genv lenv arg
          pure $ DomainIndex body' arg'
    checkC :: Show a => _ -> _ -> [Info Name a] -> TypeTW [Info Name Type]
    checkC genv lenv =
      mapM $ \e -> do
        e' <- checkE genv lenv e
        when (infoTy e' /= boolT) $
          tell
            [ CspError
                (TypeError (infoTy e') boolT)
                "constraints should always be of type boolean"
                (Just $ infoLoc e)
            ]
        pure e'
    checkE :: Show oty => _ -> LEnv -> Info Name oty -> TypeTW (Info Name Type)
    checkE genv lenv Info {infoLoc, inner} =
      case inner of
        Binop op x y -> do
          xty <- checkE genv lenv x
          yty <- checkE genv lenv y
          resT <-
            checkOp
              (infoTy xty)
              (Ast.infoLoc xty)
              (infoTy yty)
              (Ast.infoLoc yty)
              op
          pure $ Info infoLoc resT $ Binop op xty yty
        UnOp op x -> do
          xty <- checkE genv lenv x
          resT <- checkUOp (Ast.infoLoc xty) (infoTy xty) op
          pure $ Info infoLoc resT $ UnOp op xty
        Var n ->
          case M.lookup n lenv of
            Just t -> pure $ Info infoLoc t $ Var n
            Nothing ->
              throwError
                [CspError (NameError $ originalName n) "" (Just infoLoc)]
        Lit l ->
          case l of
            LitBool _ -> pure $ Info infoLoc boolT $ Lit l
            LitInt _  -> pure $ Info infoLoc intT $ Lit l
        Index body arg -> do
          bty <- checkE genv lenv body
          argty <- checkE genv lenv arg
          case infoTy bty of
            Type _ ->
              throwError
                [ CspError
                    (TypeError (TypeArray anyT 0) (infoTy bty))
                    ("The base of an index should always be an array" ++
                     show bty)
                    (Just $ Ast.infoLoc bty)
                ]
            TypeArray t size -> do
              checkRange genv lenv argty
                        -- TODO
                        -- (min, max) <- checkRange genv lenv argty
                        -- when (min < 0 || max >= size) $ throwError "out of range"
              case infoTy argty of
                Type "integer" -> pure $ Info infoLoc t $ Index bty argty
                Type "range" ->
                  pure $ Info infoLoc (TypeArray t (-1)) $ Index bty argty
                ty ->
                  throwError
                    [ CspError
                        (IndexError ty)
                        "The argument of an index should always be a range or integer"
                        (Just (Ast.infoLoc argty))
                    ]
        Call f args -> do
          argsT <- mapM (checkE genv lenv) args
          fT <- checkBuildin genv lenv argsT infoLoc f
          pure $ Info infoLoc fT $ Call f argsT
        Solve f args -> do
          argsT <- mapM (checkE genv lenv) args
          let problem = fromJust $ M.lookup f genv
              problemArgs = concat $ map (\(ns, ty) -> replicate (length ns) ty) $ input problem
          unless (length problemArgs == length argsT) $ tell [CspError (InvalidArgN f (length problemArgs) (length argsT)) "" (Just infoLoc)]
          mapM
            (\(expected, t) -> checkType (Ast.infoLoc t) expected (infoTy t) "") $
            zip problemArgs argsT
          pure $ Info infoLoc boolT $ Solve f argsT
        Forall n values body -> do
          valuesT <- mapM (checkE genv lenv) values
          when (any (not . (`elem` [rangeT, intT]) . infoTy) valuesT) $
            tell
              [ otherErr $
                (show . Ast.infoLoc . head) valuesT ++
                ": expected range/integer"
              ]
          let newEnv = M.insert n intT lenv
          bodyT <- mapM (checkE genv newEnv) body
          unless (alleq . map infoTy $ bodyT) $
            tell [otherErr "body of forall do not all have the same type"]
          let ty =
                if null bodyT
                  then Type "unit"
                  else infoTy (bodyT !! 0)
          pure $ Info infoLoc ty $ Forall n valuesT bodyT
    checkBuildin ::
         _ -> LEnv -> [Info Name Type] -> AlexPosn -> String -> TypeTW Type
    checkBuildin genv lenv args loc f
      | f == "abs" = do
        if (length args /= 1)
          then tell [CspError (InvalidArgN "abs" 1 (length args)) "" (Just loc)]
          else do
            let [arg] = args
            checkType loc (infoTy arg) intT ""
        pure intT
      | f `elem` ["alldiff", "all", "any"] = do
        unless (alleq . map infoTy $ args) $
          tell [otherErr $ "arguments to " ++ f ++ " do not have the same type"]
        pure boolT
      | otherwise = error $ f ++ " " ++ "not implemented"
    alleq []     = True
    alleq (x:xs) = all (x ==) xs
    checkRange genv lenv e = do
      when (infoTy e /= rangeT && infoTy e /= intT) $
        throwError
          [ CspError
              (IndexError (infoTy e))
              "The argument of an index should always be a range or integer"
              (Just (Ast.infoLoc e))
          ]
      pure ()
    intT = Type "integer"
    boolT = Type "boolean"
    rangeT = Type "range"
    anyT = Type "Any"
    checkOp l lloc r rloc op
      | elem op [Plus, Minus, Times, Div, Mod] = do
        checkType lloc intT l ""
        checkType rloc intT r ""
        pure intT
      | elem op [Eq, Neq, Lt, Le, Gt, Ge] = do
        checkType lloc l r "left and right part are not of the same type"
        pure boolT
      | elem op [Range] = do
        checkType lloc intT l "left range"
        checkType rloc intT r "right range"
        pure rangeT
      | elem op [RangeConcat] = do
        unless (l `elem` [rangeT, intT]) $
          tell [CspError (TypeError l rangeT) "left" (Just lloc)]
        unless (r `elem` [rangeT, intT]) $
          tell [CspError (TypeError r rangeT) "right" (Just rloc)]
        pure rangeT
    checkUOp loc x Neg = do
      checkType loc intT x ""
      pure intT
    checkUOp loc x Not = do
      checkType loc boolT x ""
      pure boolT
    checkType :: AlexPosn -> Type -> Type -> String -> TypeTW ()
    checkType loc xty yty msg =
      when (xty /= yty) . tell $ [CspError (TypeError xty yty) msg (Just loc)]
