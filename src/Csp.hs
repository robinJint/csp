module Csp
  ( run
  , runStdin
  , runFile
  ) where

import           Control.Monad.Except
import qualified Data.ByteString.Lazy as BL
import qualified System.IO            as IO

import           Csp.Base             (runAlex2)
import           Csp.Compile
import           Csp.Error
import           Csp.Lexer
import           Csp.Parser
import           Csp.Syntax
import           Csp.Transform
import           Csp.Typecheck
import           Csp.Solve

run :: String -> BL.ByteString -> IO ()
run filename input = do
  res <- f
  case res of
    Left errs -> mapM_ (putStr . prettyErr input filename) errs
    Right ast -> do
      putStrLn $ compile ast

      solutions <- solveAst ast
      case solutions of
        Just out -> putStrLn "solutions:" >> print out
        Nothing -> putStrLn "no solutions"
  where
    f =
      runExceptT $
      withExceptT (\x -> [CspError ParseError x Nothing]) (parseSyntax input) >>=
      mapM (syntaxToAst >=> nameAst) >>=
      typecheckAst >>=
      unify
    -- pure $ concat result

-- run :: BL.ByteString -> IO (Either String String)
-- run input = runExceptT $ do
--     syntax <- liftEither $ runAlex2 input parseCSP
--     let ast = map (naming . fmap shuntingyard) syntax
--         result = map compile ast
-- runStdin :: IO (Either String String)
runStdin = BL.getContents >>= run "stdin"

-- runFile :: String -> IO (Either String String)
runFile fileName = BL.readFile fileName >>= run fileName -- printRes :: Either String String -> IO ()
-- printRes res = case res of
    -- Right x -> putStrLn x
    -- Left e -> putStrLn e
